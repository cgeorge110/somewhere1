var mongo = require('mongodb');
var express = require('express');
var monk = require('monk');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multipart = require('multiparty');
var routes = require('./routes/index');
var users = require('./routes/users');
var articles = require('./routes/articles');

var db =  monk('localhost:27017/trip');
var everyauth = require('everyauth'); // authentication middleware 

everyauth.debug = true;
everyauth.everymodule.handleLogout( function (req, res) {
  req.session = null; // or req.session.destroy()

  this.redirect(res, this.logoutRedirectPath());
});
everyauth.everymodule.findUserById( function (user, callback) {
  callback(user)
});


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(everyauth.middleware(app)); // authentication middleware
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(__dirname + '/bower_components/bootstrap/dist'));
app.use(express.static(__dirname + '/bower_components/tinymce/js'));

// Make db viewable throughout
app.use(function(req,res,next){
    req.db = db;
    next();
});


// Setting the routes and which modules to use
// ===========================================
app.use('/', routes);
app.use('/users', users);
app.use('/articles', articles);
// ===========================================

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;

var server = app.listen(3000, function() {
    console.log('Listening on port %d', server.address().port);
});
