var express = require('express');
var articles = express.Router({});
var mongo = require('mongodb');
var monk = require('monk');
var db =  monk('localhost:27017/trip');
var articlesCollection = db.get('articles');
var fs = require('fs');
var gm = require('gm');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

/* GET users listing. */
articles.get('/', function(req, res) {
	articlesCollection.find({}, function(error, articles){
		if (error) return next(error);
		res.render('articles/index', {
			title: "Hello from articles!",
			articles: articles
		})
	});
});

/* GET new article form */
articles.get('/new', function(req, res) {
	res.render('articles/new');
});

/* POST new article form */
articles.post('/new', multipartMiddleware, function(req, res, next){
	
	console.log(req.body.title);
	if (!req.body.title || !req.body.subheading || !req.body.text ) {
	    return res.render('articles/new', {error: "Fill title, slug and text."});
	}
	console.log('path: ' + req.files.image.path);
		console.log('name: ' + req.files.image.name);
		fs.readFile(req.files.image.path, function(err,data){
		//See if an image actually came through
		var imageName = req.files.image.name;
		console.log(imageName);
		
		//if there's an error -- at this time, do we assume this means that no image was uploaded?
		if(!imageName){
			//do something at some point -- maybe?
		}
		else
		{
			// Set paths for each of the destination folders for main image
			var ogPath = imagePath("og",imageName);
			var largePath = imagePath("large", imageName);
			var mediumPath = imagePath("medium", imageName);
			var smallPath = imagePath("small", imageName);
			
			// Write the file to the large path
			fs.writeFile(ogPath, data, function(err) {
				console.log(data);
				console.log('successfully wrote image');
				
			});
			// Write file to large path 
			gm(ogPath)
			.resize(1400, 450)
			.noProfile()
			.write(largePath, function (err) {
			  if (!err) console.log('done');
			});
			
			// Write file to medium path 
			gm(ogPath)
			.resize(960, 300)
			.noProfile()
			.write(mediumPath, function (err) {
			  if (!err) console.log('done');
			});
			
			// Write file to small path 
			gm(ogPath)
			.resize(320, 205)
			.noProfile()
			.write(smallPath, function (err) {
			  if (!err) console.log('done');
			});
		}
		
	})
	var slug = slugify(req.body.title);
	var article = {
		title: req.body.title,
		slug: slug,
		subheading: req.body.subheading,
		text: req.body.text,
		published: req.body.published,
		header: req.files.image.name
	};
	articlesCollection.insert(article, function(error, articleResponse){
		if (error) return next(error);
		res.render('articles/index', {
			error: 'Article added.',
			title: "Hello from articles!"
		});
	});
	//res.send('posted that shit, homie')
});

/* GET article post */
articles.get('/:slug', function(req, res){
	console.log(req.params.slug);
	articlesCollection.findOne({ slug: req.params.slug }, function(error, article){
		if (error) return next(error);
		//if(!article.published) return res.send(401); // to-do: if admin, allow view of unpublished
		res.render('articles/article', article);
	});
});

/* GET article edit */
articles.get('/:slug/edit', function(req, res){
	console.log(req.params.slug);
	articlesCollection.findOne({ slug: req.params.slug }, function(error, article){
		if (error) return next(error);
		//if(!article.published) return res.send(401); // to-do: if admin, allow view of unpublished
		res.render('articles/edit', article);
	});
});

/* PUT new article form */
articles.post('/edit', function(req, res, next){
	console.log(req.body.title);
	var slug = slugify(req.body.title);
	var id = req.body.id;
	articlesCollection.updateById(req.body.id, {
		title: req.body.title,
		slug: slug,
		subheading: req.body.subheading,
		text: req.body.text,
		published: req.body.published,
		updated: Date.now()
	},
	function(error){
		if(error) {
        	return console.log('update error', err);
        }
		
	});
	/*articlesCollection.findOne({ _id: id }, function(error, article){
		console.log(error);
		if (error) return next(error);
		//if(!article.published) return res.send(401); // to-do: if admin, allow view of unpublished
		res.render('articles/article', article);
	});*/
	//res.send('posted that shit, homie')
	res.redirect('/articles/'+slug);
});


/* Slugify -- create a custom slug based on article title */
function slugify(text) {
	return text.toString().toLowerCase()
      .replace(/\s+/g, '-')        // Replace spaces with -
      .replace(/[^\w\-]+/g, '')   // Remove all non-word chars
      .replace(/\-\-+/g, '-')      // Replace multiple - with single -
      .replace(/^-+/, '')          // Trim - from start of text
      .replace(/-+$/, '');         // Trim - from end of text
  }

function imagePath(size,imageName) {
	return __dirname + "/public/uploads/" + size + "/" + imageName;
}	


module.exports = articles;
