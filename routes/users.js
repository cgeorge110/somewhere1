var express = require('express');
var users = express.Router({});
var mongo = require('mongodb');
var monk = require('monk');
var db =  monk('localhost:27017/trip');
var usersCollection = db.get('users');

/* GET users listing. */
users.get('/', function(req, res) {
  //TO DO -- if authenticated, show list of users...if not, show login
  res.render('users/login');
});

/* POST user login */
users.post('/', function(req, res, next) {
  if (!req.body.email || !req.body.password)
    return res.render('users/login', {error: "Please enter your email and password."});
  usersCollection.findOne({
    email: req.body.email,
    password: req.body.password
  }, function(error, user){
	console.log("error: " + error);
	if (error) return next(error);
    if (!user) return res.render('users/login', {error: "Incorrect email or password."});
    req.session.user = user;
    req.session.admin = user.admin;
    res.redirect('/admin');
  });
});

/* GET users listing. */
users.get('/logout', function(req, res) {
  req.session = null; // kill the session
  res.redirect('/'); // go back to homepage
});

module.exports = users;
